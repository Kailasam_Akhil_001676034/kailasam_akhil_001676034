/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.CustomerRole;

import UserInterface.SalesPersonRole.*;
import Business.Business;
import Business.Customer;
import Business.CustomerDirectory;
import Business.Employee;
import Business.EmployeeDirectory;
import Business.MasterOrderDirectory;
import Business.OrderItem;
import Business.Product;
import Business.ProductCatalog;
import Business.SalesPerson;
import java.awt.CardLayout;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author akki
 */
public class BrowseProductCatalogJPanel extends javax.swing.JPanel {
    
    private JPanel userProcessContainer;
    private Business business;
    private ProductCatalog productCatalog;
    private MasterOrderDirectory masterOrderDirectory;
    private CustomerDirectory customerDirectory;
    private EmployeeDirectory employeeDirectory;
    private SalesPerson salesPerson;
    private Customer customer;
    
    
    public BrowseProductCatalogJPanel(JPanel userProcessContainer, Business business,Customer customer) {
        initComponents();
        this.userProcessContainer= userProcessContainer;
        this.business=business;
        this.productCatalog = business.getProductCatalog();
        this.masterOrderDirectory = business.getMasterOrderDirectory();
        this.customerDirectory = business.getCustomerDirectory();
        this.employeeDirectory = business.getEmployeeDirectory();
        this.salesPerson=salesPerson;
        this.customer=customer;
        
        
        populateProductTable();
        btnReset.setVisible(false);
    }

    
        public void populateProductTable() {
        DefaultTableModel dtm = (DefaultTableModel) tblProductCatalog.getModel();
        
        dtm.setRowCount(0);
        
            for (Product product : productCatalog.getProductList()) {
                Object row[] = new Object[8];
                row[0] = product;
                row[1] = product.getProductName();
                row[2] = product.getProductCategory();
                row[3] = product.getProductBrand();
                row[4] = product.getProductTP();
                row[5] = product.getProductWeight();
                row[6] = product.getProductAvailableQuantity();
                row[7] = product.getProductDescription();
                   
                
                dtm.addRow(row);
                
            }
        
    }
    
    private void refreshProductTable(String keyWord) {
        int rowCount = tblProductCatalog.getRowCount();
        DefaultTableModel model = (DefaultTableModel) tblProductCatalog.getModel();
        for (int i = rowCount - 1; i >= 0; i--) {
            model.removeRow(i);
        }

        
            for (Product product : productCatalog.getProductList()) {
                if (keyWord.equals(product.getProductName())) {
                Object row[] = new Object[8];
                row[0] = product;
                row[1] = product.getProductName();
                row[2] = product.getProductCategory();
                row[3] = product.getProductBrand();
                row[4] = product.getProductTP();
                row[5] = product.getProductWeight();
                row[6] = product.getProductAvailableQuantity();
                row[7] = product.getProductDescription();
                    model.addRow(row);
                    btnReset.setVisible(true);
                }
            }
            
            if(tblProductCatalog.getRowCount()==0){
                JOptionPane.showMessageDialog(null, "No matches found", "INFO", JOptionPane.INFORMATION_MESSAGE);
                populateProductTable();
                txtSearchKeyWord.setText("");
            }
        

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnReset = new javax.swing.JButton();
        txtSearchKeyWord = new javax.swing.JTextField();
        btnSearchProduct = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblProductCatalog = new javax.swing.JTable();
        jLabel9 = new javax.swing.JLabel();

        btnReset.setText("Reset Search");
        btnReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResetActionPerformed(evt);
            }
        });

        btnSearchProduct.setText("Search Product By Name");
        btnSearchProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchProductActionPerformed(evt);
            }
        });

        btnBack.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnBack.setText("<< Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        tblProductCatalog.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Product ID", "Product Name", "Category", "Brand", "Listed Price", "Weight", "Available Quantity", "Description"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblProductCatalog.getTableHeader().setReorderingAllowed(false);
        jScrollPane3.setViewportView(tblProductCatalog);

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel9.setText("Product Catalog:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(736, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 814, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(20, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnReset)
                        .addGap(31, 31, 31)
                        .addComponent(txtSearchKeyWord, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnSearchProduct)
                        .addGap(131, 131, 131))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(58, 58, 58)
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(20, 20, 20))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnSearchProduct)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtSearchKeyWord, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnReset)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 48, Short.MAX_VALUE)
                .addComponent(btnBack)
                .addGap(192, 192, 192))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResetActionPerformed
        // TODO add your handling code here:

        populateProductTable();
        txtSearchKeyWord.setText("");
        btnReset.setVisible(false);
    }//GEN-LAST:event_btnResetActionPerformed

    private void btnSearchProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchProductActionPerformed
        String keyWord = txtSearchKeyWord.getText();
        refreshProductTable(keyWord);
    }//GEN-LAST:event_btnSearchProductActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
        
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnReset;
    private javax.swing.JButton btnSearchProduct;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable tblProductCatalog;
    private javax.swing.JTextField txtSearchKeyWord;
    // End of variables declaration//GEN-END:variables
}
