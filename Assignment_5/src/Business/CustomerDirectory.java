/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author akki
 */
public class CustomerDirectory {
    
    private ArrayList<Customer> customerList;

    public CustomerDirectory() {
    customerList = new ArrayList<Customer>();
    }
    
    public ArrayList<Customer> getCustomerList(){
        return customerList;
    }
    
    
    public Customer addCustomer(){
        Customer p = new Customer();
        customerList.add(p);
        return p;
    }
    
    public void removeCustomer(Customer p){
        customerList.remove(p);
    }
    
    public Customer searchCustomer(String name){
        for (Customer c : customerList) {
            if(c.getPerson().getPersonName().equals(name)){
                return c;
            }
        }
        return null;
    }
}
