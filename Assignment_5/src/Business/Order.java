/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

/**
 *
 * @author akki
 */
public class Order {
    
    private ArrayList<OrderItem> orderItemList;
    
    private String orderID;
    private String orderStatus;
    private int orderTotalAmount;
    private int orderTotalQuantity;
    private double orderSalesPersonCommission;
    private String orderIssueDate;
    private String orderCompletionDate;
    //private String orderShippingDate;
    private String orderComments;
    private String orderAssignedTo;
    private String orderRequestedBy;
    
    private Customer customer;
    private Employee employee;
    
    
    
    
    
    public Order() {
    orderItemList = new ArrayList<OrderItem>();
     orderID = UUID.randomUUID().toString();
     orderTotalQuantity=0;
    }
    
    @Override
    public String toString() {
        return this.orderID; 
    }
    
    
    public ArrayList<OrderItem> getOrderItemList(){
        return orderItemList;
    }
    
    

        public OrderItem addOrderItem(Product p, int q, int price) {
        OrderItem o = new OrderItem();
        o.setProduct(p);
        o.setOrderItemQuantity(q);
        o.setProductEP(price);
        orderItemList.add(o);
        return o;
    }
    
    public void removeOrderItem(OrderItem p){
        orderItemList.remove(p);
    }
    
    public OrderItem searchOrderItem(String name){
        for (OrderItem e : orderItemList) {
            if(e.getProduct().getProductName().equals(name)){
                return e;
            }
        }
        return null;
    }

    public ArrayList<OrderItem> getOrderList() {
        return orderItemList;
    }

    public void setOrderList(ArrayList<OrderItem> orderItemList) {
        this.orderItemList = orderItemList;
    }
    
    
    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public int getOrderTotalAmount() {
        return orderTotalAmount;
    }

    public void setOrderTotalAmount(int orderTotalAmount) {
        this.orderTotalAmount = orderTotalAmount;
    }

    
    public double getOrderSalesPersonCommission() {
        return orderSalesPersonCommission;
    }

    public void setOrderSalesPersonCommission(double orderSalesPersonCommission) {
        this.orderSalesPersonCommission = orderSalesPersonCommission;
    }

    public String getOrderIssueDate() {
        return orderIssueDate;
    }

    public void setOrderIssueDate(String orderIssueDate) {
        this.orderIssueDate = orderIssueDate;
    }

    public String getOrderCompletionDate() {
        return orderCompletionDate;
    }

    public void setOrderCompletionDate(String orderCompletionDate) {
        this.orderCompletionDate = orderCompletionDate;
    }
//
//    public String getOrderShippingDate() {
//        return orderShippingDate;
//    }
//
//    public void setOrderShippingDate(String orderShippingDate) {
//        this.orderShippingDate = orderShippingDate;
//    }

    public String getOrderComments() {
        return orderComments;
    }

    public void setOrderComments(String orderComments) {
        this.orderComments = orderComments;
    }

    public String getOrderAssignedTo() {
        return orderAssignedTo;
    }

    public void setOrderAssignedTo(String orderAssignedTo) {
        this.orderAssignedTo = orderAssignedTo;
    }

    public String getOrderRequestedBy() {
        return orderRequestedBy;
    }

    public void setOrderRequestedBy(String orderRequestedBy) {
        this.orderRequestedBy = orderRequestedBy;
    }

    public int getOrderTotalQuantity() {
        return orderTotalQuantity;
    }

    public void setOrderTotalQuantity(int orderTotalQuantity) {
        this.orderTotalQuantity = orderTotalQuantity;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    
    
    
}
