/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author akki
 */
public class MasterOrderDirectory {
    private ArrayList<Order> orderList;

    public MasterOrderDirectory() {
    orderList = new ArrayList<Order>();
    }
    
    public ArrayList<Order> getOrderList() {
        return orderList;
    }

    public Order addOrder(Order o){
        
        orderList.add(o);
        return o;
    }
    
    public void removeOrder(Order p){
        orderList.remove(p);
    }
    
    public Order searchOrder(String orderid){
        for (Order e : orderList) {
            if(e.getOrderID().equals(orderid)){
                return e;
            }
        }
        return null;
    }

    
}
