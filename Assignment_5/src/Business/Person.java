/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.UUID;

/**
 *
 * @author akki
 */
public class Person{
    
  
    private String personID;
    private String personName;
    private String personRole;
    private String personSpecificRole;
    private String personGender;
    private int personAge;
    private int personContactNumber;
    private String personEmailID;
    private String personAddress;
    
    public Person(){
        personID = UUID.randomUUID().toString();
        
        
    }
    
    
    @Override
    public String toString(){
        return this.personName;
    }
    
    
    
    public String getPersonID() {
        return personID;
    }

    public void setPersonID(String personID) {
        this.personID = personID;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public int getPersonAge() {
        return personAge;
    }

    public void setPersonAge(int personAge) {
        this.personAge = personAge;
    }

    public int getPersonContactNumber() {
        return personContactNumber;
    }

    public void setPersonContactNumber(int personContactNumber) {
        this.personContactNumber = personContactNumber;
    }

    public String getPersonEmailID() {
        return personEmailID;
    }

    public void setPersonEmailID(String personEmailID) {
        this.personEmailID = personEmailID;
    }

    public String getPersonAddress() {
        return personAddress;
    }

    public void setPersonAddress(String personAddress) {
        this.personAddress = personAddress;
    }

    public String getPersonGender() {
        return personGender;
    }

    public void setPersonGender(String personGender) {
        this.personGender = personGender;
    }

    public String getPersonRole() {
        return personRole;
    }

    public void setPersonRole(String personRole) {
        this.personRole = personRole;
    }

    public String getPersonSpecificRole() {
        return personSpecificRole;
    }

    public void setPersonSpecificRole(String personSpecificRole) {
        this.personSpecificRole = personSpecificRole;
    }
    
    
}
