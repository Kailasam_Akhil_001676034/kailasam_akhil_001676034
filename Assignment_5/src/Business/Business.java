/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author akki
 */
public class Business {
    private static Business business;
    
    
    private ProductCatalog productCatalog;
    private MasterOrderDirectory masterOrderDirectory;
    private CustomerDirectory customerDirectory;
    private EmployeeDirectory employeeDirectory;
    private PersonDirectory personDirectory;
    private BIAnalytics bIAnalytics;
    
    public Business() {
        
        
        productCatalog = new ProductCatalog();
        masterOrderDirectory = new MasterOrderDirectory();
        customerDirectory = new CustomerDirectory();
        employeeDirectory = new EmployeeDirectory();
        personDirectory = new PersonDirectory();
        bIAnalytics = new BIAnalytics();
    }
    
    
    

    public ProductCatalog getProductCatalog() {
        return productCatalog;
    }

    public void setProductCatalog(ProductCatalog productCatalog) {
        this.productCatalog = productCatalog;
    }

    public MasterOrderDirectory getMasterOrderDirectory() {
        return masterOrderDirectory;
    }

    public void setMasterOrderDirectory(MasterOrderDirectory masterOrderDirectory) {
        this.masterOrderDirectory = masterOrderDirectory;
    }

    public CustomerDirectory getCustomerDirectory() {
        return customerDirectory;
    }

    public void setCustomerDirectory(CustomerDirectory customerDirectory) {
        this.customerDirectory = customerDirectory;
    }

    public EmployeeDirectory getEmployeeDirectory() {
        return employeeDirectory;
    }

    public void setEmployeeDirectory(EmployeeDirectory employeeDirectory) {
        this.employeeDirectory = employeeDirectory;
    }

    public PersonDirectory getPersonDirectory() {
        return personDirectory;
    }

    public void setPersonDirectory(PersonDirectory personDirectory) {
        this.personDirectory = personDirectory;
    }

    public BIAnalytics getbIAnalytics() {
        return bIAnalytics;
    }

    public void setbIAnalytics(BIAnalytics bIAnalytics) {
        this.bIAnalytics = bIAnalytics;
    }


    
}
