/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author akki
 */
public class Customer {
    private Person person;
    private int customerTotalSalesVolume;
    
    public Customer(){
        person = new Person();
        person.setPersonRole("Customer");
        person.setPersonSpecificRole("NA");
        customerTotalSalesVolume=0;
        
    }
    
    
    @Override
    public String toString() {
        return this.person.getPersonName(); 
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public int getCustomerTotalSalesVolume() {
        return customerTotalSalesVolume;
    }

    public void setCustomerTotalSalesVolume(int customerTotalSalesVolume) {
        this.customerTotalSalesVolume = customerTotalSalesVolume;
    }
    
    
    
}
