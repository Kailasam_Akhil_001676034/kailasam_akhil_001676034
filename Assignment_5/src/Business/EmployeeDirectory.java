/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author akki
 */
public class EmployeeDirectory {
    private ArrayList<Employee> employeeList;

    public EmployeeDirectory() {
    employeeList = new ArrayList<Employee>();
    }
    
    public ArrayList<Employee> getEmployeeList(){
        return employeeList;
    }
    
    
    public Employee addEmployee(){
        Employee p = new Employee();
        employeeList.add(p);
        return p;
    }
    
    public void removeEmployee(Employee p){
        employeeList.remove(p);
    }
    
    public Employee searchEmployee(String name){
        for (Employee e : employeeList) {
            if(e.getPerson().getPersonName().equals(name)){
                return e;
            }
        }
        return null;
    }
}
