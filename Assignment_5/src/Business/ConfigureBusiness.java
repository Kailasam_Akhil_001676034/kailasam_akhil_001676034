/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author akki
 */
public class ConfigureBusiness {
    public static Business initializeBusiness( ) {
        Business business = Business.getInstance();
        
//        Employee employee = business.getEmployeeDirectory().addEmployee();
//        employee.setFirstName("Admin");
//        employee.setLastName("Admin");
//        employee.setOrganization("NEU");
//        UserAccount userAccount = business.getUserAccountDirectory().AddUserAccount();
//        userAccount.setUserName("admin");
//        userAccount.setPassword("admin");
//        userAccount.setPerson(employee.getPerson());
//        userAccount.setRole(UserAccount.ADMIN_ROLE);
//        userAccount.setIsActive("Yes");
        
        
        
        Product p1 = business.getProductCatalog().addProduct();
        p1.setProductName("Printer");
        p1.setProductCategory("Stationary");
        p1.setProductBrand("Xerox");
        p1.setProductFP(100);
        p1.setProductCP(300);
        p1.setProductTP(200);
        p1.setProductAvailableQuantity(6);
        p1.setProductWeight(40);
        p1.setProductDescription("jjkljljkljl");
 
        Product p2 = business.getProductCatalog().addProduct();
        p2.setProductName("Scanner");
        p2.setProductCategory("Stationary");
        p2.setProductBrand("Xerox");
        p2.setProductFP(500);
        p2.setProductCP(2000);
        p2.setProductTP(1000);
        p2.setProductAvailableQuantity(15);
        p2.setProductWeight(40);
        p2.setProductDescription("jjkljljkljl");
        
        Employee e= business.getEmployeeDirectory().addEmployee();
        e.getPerson().setPersonName("a");
        e.getPerson().setPersonGender("Male");
        e.getPerson().setPersonAge(22);
        e.getPerson().setPersonContactNumber(1231231231);
        e.getPerson().setPersonEmailID("jkl.jkl@jkl.jkl");
        e.getPerson().setPersonAddress("kjkjkljlkjl");
        
        Employee e2= business.getEmployeeDirectory().addEmployee();
        e2.getPerson().setPersonName("b");
        e2.getPerson().setPersonGender("Male");
        e2.getPerson().setPersonAge(22);
        e2.getPerson().setPersonContactNumber(1231231232);
        e2.getPerson().setPersonEmailID("jkl.jkl@jkl.jkl");
        e2.getPerson().setPersonAddress("kjkjkljlkjl");
        
        Employee e3= business.getEmployeeDirectory().addEmployee();
        e3.getPerson().setPersonName("c");
        e3.getPerson().setPersonGender("Male");
        e3.getPerson().setPersonAge(22);
        e3.getPerson().setPersonContactNumber(1231231233);
        e3.getPerson().setPersonEmailID("jkl.jkl@jkl.jkl");
        e3.getPerson().setPersonAddress("kjkjkljlkjl");
        
        Employee e4= business.getEmployeeDirectory().addEmployee();
        e4.getPerson().setPersonName("d");
        e4.getPerson().setPersonGender("Male");
        e4.getPerson().setPersonAge(22);
        e4.getPerson().setPersonContactNumber(1231231234);
        e4.getPerson().setPersonEmailID("jkl.jkl@jkl.jkl");
        e4.getPerson().setPersonAddress("kjkjkljlkjl");
        
        Employee e5= business.getEmployeeDirectory().addEmployee();
        e5.getPerson().setPersonName("e");
        e5.getPerson().setPersonGender("Male");
        e5.getPerson().setPersonAge(22);
        e5.getPerson().setPersonContactNumber(1231231235);
        e5.getPerson().setPersonEmailID("jkl.jkl@jkl.jkl");
        e5.getPerson().setPersonAddress("kjkjkljlkjl");
        
        Customer c= business.getCustomerDirectory().addCustomer();
        c.getPerson().setPersonName("x");
        c.getPerson().setPersonGender("Female");
        c.getPerson().setPersonAge(22);
        c.getPerson().setPersonContactNumber(1231231230);
        c.getPerson().setPersonEmailID("jkl.jkl@jkl.uio");
        c.getPerson().setPersonAddress("kjkjkljlkjl");
        
        Customer c2= business.getCustomerDirectory().addCustomer();
        c2.getPerson().setPersonName("y");
        c2.getPerson().setPersonGender("Female");
        c2.getPerson().setPersonAge(22);
        c2.getPerson().setPersonContactNumber(1231231239);
        c2.getPerson().setPersonEmailID("jkl.jkl@jkl.uio");
        c2.getPerson().setPersonAddress("kjkjkljlkjl");
        
        Customer c3= business.getCustomerDirectory().addCustomer();
        c3.getPerson().setPersonName("z");
        c3.getPerson().setPersonGender("Female");
        c3.getPerson().setPersonAge(22);
        c3.getPerson().setPersonContactNumber(1231231289);
        c3.getPerson().setPersonEmailID("jkl.jkl@jkl.uio");
        c3.getPerson().setPersonAddress("kjkjkljlkjl");
        
        Order o= new Order();
        business.getMasterOrderDirectory().addOrder(o);
        OrderItem oi1 = o.addOrderItem(p2, 1, 1100);
        o.setOrderStatus("Pending for Assignment");
        o.setOrderAssignedTo("NA");
        o.setOrderTotalAmount(1200);
        o.setOrderSalesPersonCommission(50);

        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
        Date dateobj = new Date();

        o.setOrderIssueDate(df.format(dateobj));
        o.setOrderCompletionDate("NA");
        
//         Order o2= new Order();
//        masterOrderDirectory.addOrder(o2);
//        OrderItem oi2 = o2.addOrderItem(p2, 2, 900);
//        
//         Order o3= new Order();
//        masterOrderDirectory.addOrder(o3);
//        OrderItem oi3 = o3.addOrderItem(p2, 3, 1300);
//        
//         Order o4= new Order();
//        masterOrderDirectory.addOrder(o4);
//        OrderItem oi4 = o4.addOrderItem(p2, 4, 900);
//        
//         Order o5= new Order();
//        masterOrderDirectory.addOrder(o5);
//        OrderItem oi5 = o5.addOrderItem(p2, 5, 1500);
        
        
            return business;
        
    }
    

      
    
}
