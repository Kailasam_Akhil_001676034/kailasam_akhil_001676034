/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author akki
 */
public class Employee {
    private Person person;
    private String employeePassword;
    private SalesPerson salesPerson;
    
    private int employeeTotalSalesVolume;
    private double employeeTotalSalesCommission;
    private int aboveTargetPrice;  
    private int belowTargetPrice;
    
    
    public Employee(){
        person = new Person();
        person.setPersonRole("Employee");
        person.setPersonSpecificRole("SalesPerson");
        employeeTotalSalesVolume=0;
    employeeTotalSalesCommission=0;
    aboveTargetPrice=0;  
    belowTargetPrice=0;
        
    }
    
    @Override
    public String toString() {
        return this.person.getPersonName(); 
    }

    
    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public String getEmployeePassword() {
        return employeePassword;
    }

    public void setEmployeePassword(String employeePassword) {
        this.employeePassword = employeePassword;
    }

    public SalesPerson getSalesPerson() {
        return salesPerson;
    }

    public void setSalesPerson(SalesPerson salesPerson) {
        this.salesPerson = salesPerson;
    }

    public int getEmployeeTotalSalesVolume() {
        return employeeTotalSalesVolume;
    }

    public void setEmployeeTotalSalesVolume(int employeeTotalSalesVolume) {
        this.employeeTotalSalesVolume = employeeTotalSalesVolume;
    }

    public double getEmployeeTotalSalesCommission() {
        return employeeTotalSalesCommission;
    }

    public void setEmployeeTotalSalesCommission(double employeeTotalSalesCommission) {
        this.employeeTotalSalesCommission = employeeTotalSalesCommission;
    }
    
    


    public int getAboveTargetPrice() {
        return aboveTargetPrice;
    }

    public void setAboveTargetPrice(int aboveTargetPrice) {
        this.aboveTargetPrice = aboveTargetPrice;
    }

    public int getBelowTargetPrice() {
        return belowTargetPrice;
    }

    public void setBelowTargetPrice(int belowTargetPrice) {
        this.belowTargetPrice = belowTargetPrice;
    }
    
    
    
}
