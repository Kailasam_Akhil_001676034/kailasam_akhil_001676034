/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.UUID;

/**
 *
 * @author akki
 */
public class Product {
    
    private String productID;
    private String productName;
    private String productCategory;
    private String productBrand;
    private int productFP;
    private int productCP;
    private int productTP;
    private int productWeight;
    private int productAvailableQuantity;
    private String productDescription;
    
    private int noOfCustomersOrdered;
    private int totQuantityOrdered;
    private double popularityRating;
    
    
    public Product(){
        productID = UUID.randomUUID().toString();
        noOfCustomersOrdered=0;
        totQuantityOrdered=0;
        popularityRating=0;
    }
    
    @Override
    public String toString() {
        return this.productID; 
    }
    
    
    
    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public String getProductBrand() {
        return productBrand;
    }

    public void setProductBrand(String productBrand) {
        this.productBrand = productBrand;
    }

    public int getProductFP() {
        return productFP;
    }

    public void setProductFP(int productFP) {
        this.productFP = productFP;
    }

    public int getProductCP() {
        return productCP;
    }

    public void setProductCP(int productCP) {
        this.productCP = productCP;
    }

    public int getProductWeight() {
        return productWeight;
    }

    public void setProductWeight(int productWeight) {
        this.productWeight = productWeight;
    }

    public int getProductAvailableQuantity() {
        return productAvailableQuantity;
    }

    public void setProductAvailableQuantity(int productAvailableQuantity) {
        this.productAvailableQuantity = productAvailableQuantity;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public int getProductTP() {
        return productTP;
    }

    public void setProductTP(int productTP) {
        this.productTP = productTP;
    }

    public int getNoOfCustomersOrdered() {
        return noOfCustomersOrdered;
    }

    public void setNoOfCustomersOrdered(int noOfCustomersOrdered) {
        this.noOfCustomersOrdered = noOfCustomersOrdered;
    }

    public int getTotQuantityOrdered() {
        return totQuantityOrdered;
    }

    public void setTotQuantityOrdered(int totQuantityOrdered) {
        this.totQuantityOrdered = totQuantityOrdered;
    }

    public double getPopularityRating() {
        return popularityRating;
    }

    public void setPopularityRating(double popularityRating) {
        this.popularityRating = popularityRating;
    }

    
    
}
