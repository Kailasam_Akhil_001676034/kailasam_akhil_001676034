/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author akki
 */
public class OrderItem {
    
    private Product product;
    
    private int productEP;
    private int orderItemQuantity;

    public Product getProduct() {
        return product;
    }
    
    @Override
    public String toString() {
        return this.product.getProductID(); 
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    

    public int getOrderItemQuantity() {
        return orderItemQuantity;
    }

    public void setOrderItemQuantity(int orderItemQuantity) {
        this.orderItemQuantity = orderItemQuantity;
    }

    public int getProductEP() {
        return productEP;
    }

    public void setProductEP(int productEP) {
        this.productEP = productEP;
    }
    
}
