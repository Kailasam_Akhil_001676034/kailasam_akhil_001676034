/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author akki
 */
public class PersonDirectory {
    
       private ArrayList<Person> personList;
    
    public PersonDirectory(){
        personList = new ArrayList<Person>();
    
    }

    public ArrayList<Person> getPersonList() {
        return personList;
    }

    public Person addPerson()
    {
        Person p = new Person();
        personList.add(p);
        return p;
        
    }
    
    public void deletePerson(Person p)
    {
      personList.remove(p);
      
    }
    
    public Person searchPerson(int x)
    { 
    for(Person p: personList)
    {
    if(p.getMobilenumber()==x)
    { return p;   
    }
    
    
    }
    return null;
}
    
    
}
