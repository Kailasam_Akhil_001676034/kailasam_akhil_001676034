/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author akki
 */
public class Person{
    
    //private static int count =0;
    
    //private String personid;
   // private String LastName;
    
    private String name;
    private int age;
    private int mobilenumber;
    private String email;
    private String primarydoctorname;
    private String preferredpharmacy;
    

    
    //count++;
       // personid  = String.valueOf(count);
    
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    
    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getMobilenumber() {
        return mobilenumber;
    }

    public void setMobilenumber(int mobilenumber) {
        this.mobilenumber = mobilenumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPrimarydoctorname() {
        return primarydoctorname;
    }

    public void setPrimarydoctorname(String primarydoctorname) {
        this.primarydoctorname = primarydoctorname;
    }

    public String getPreferredpharmacy() {
        return preferredpharmacy;
    }

    public void setPreferredpharmacy(String preferredpharmacy) {
        this.preferredpharmacy = preferredpharmacy;
    }
    
    
    @Override
    public String toString(){
        return this.name;
    }
    
    
    
}
