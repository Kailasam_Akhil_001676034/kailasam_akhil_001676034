/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author akki
 */
public class PatientDirectory {
    
    private ArrayList<Patient> patientList;
    
   
    
    public PatientDirectory(){
        patientList = new ArrayList<Patient>();
    
    }

    public ArrayList<Patient> getPatientList() {
        return patientList;
    }

    public void addPatientProfile(Person person1)
    {
     Patient patient= new Patient();
     
        patient.setName(person1.getName());
        patient.setAge(person1.getAge());
        patient.setMobilenumber(person1.getMobilenumber());
        patient.setEmail(person1.getEmail());
        patient.setPrimarydoctorname(person1.getPrimarydoctorname());
        patient.setPreferredpharmacy(person1.getPreferredpharmacy());
        
        patientList.add(patient);
        
    }
      
    
    public void deletePatientProfile(Patient p)
    {
      patientList.remove(p);
      
    }
    
    public Patient searchPatientProfile(int mn)
    {
    for(Patient p: patientList)
    {
    if(p.getMobilenumber()== mn)
    { return p;   
    }
    
    
    }
    return null;
}   
    
    
}
