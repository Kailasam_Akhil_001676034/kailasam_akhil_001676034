package UserInterface.PatientRole;

import Business.PatientDirectory;
import Business.PersonDirectory;
import java.awt.CardLayout;
import javax.swing.JPanel;

/**
 *
 * @author Mihir Mehta / Hechen Gao
 */
public class PatientWorkAreaJPanel extends javax.swing.JPanel {
private PatientDirectory patientDirectory;
    private JPanel userProcessContainer;
    private PersonDirectory personDirectory;
    public PatientWorkAreaJPanel(PatientDirectory patientDirectory,PersonDirectory personDirectory, JPanel userProcessContainer) {
        initComponents();
        this.patientDirectory = patientDirectory;
        this.userProcessContainer = userProcessContainer;
        this.personDirectory= personDirectory;
        
    
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        btnManagePatientDirectory = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("Patient Profile Manager");

        btnManagePatientDirectory.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnManagePatientDirectory.setText("Manage Patient Directory >>");
        btnManagePatientDirectory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnManagePatientDirectoryActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(130, 130, 130)
                        .addComponent(btnManagePatientDirectory))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(111, 111, 111)
                        .addComponent(jLabel1)))
                .addContainerGap(161, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(jLabel1)
                .addGap(86, 86, 86)
                .addComponent(btnManagePatientDirectory)
                .addContainerGap(87, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnManagePatientDirectoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnManagePatientDirectoryActionPerformed
        // TODO add your handling code here:
        ManagePatientDirectoryJPanel managePersonDirectoryJPanel = new ManagePatientDirectoryJPanel( patientDirectory, personDirectory, userProcessContainer);
        userProcessContainer.add("ManagePersonDirectory",managePersonDirectoryJPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
        
    }//GEN-LAST:event_btnManagePatientDirectoryActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnManagePatientDirectory;
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables
}
