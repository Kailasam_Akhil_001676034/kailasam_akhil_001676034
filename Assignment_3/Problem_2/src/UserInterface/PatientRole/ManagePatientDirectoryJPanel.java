package UserInterface.PatientRole;

import Business.Patient;
import Business.PatientDirectory;
import Business.PersonDirectory;

import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;


/**
 *
 * @author Rushabh
 */
public class ManagePatientDirectoryJPanel extends javax.swing.JPanel {

    
  
    /** Creates new form ManagePatientDirectoryJPanel */
    private PatientDirectory patientDirectory;
    private JPanel userProcessContainer;
    private PersonDirectory personDirectory;
    
    public ManagePatientDirectoryJPanel(PatientDirectory patientDirectory,PersonDirectory personDirectory, JPanel userProcessContainer) {
        initComponents();
        this.patientDirectory = patientDirectory;
        this.userProcessContainer = userProcessContainer;
        this.personDirectory= personDirectory;
        
    
       refreshTable();
    
    }
private void refreshTable()
    {  DefaultTableModel dtm = (DefaultTableModel) tblPatientDirectory.getModel();
        dtm.setRowCount(0);
        
        for(Patient p : patientDirectory.getPatientList())
        {  Object[] row = new Object[6];
            row[0]=p;
            row[1]=p.getAge();
            row[2]=p.getMobilenumber();
            row[3]=p.getEmail();
            row[4]=p.getPrimarydoctorname();
            row[5]=p.getPreferredpharmacy();
            dtm.addRow(row);
            
            
            
        }
    }
    
    
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLayeredPane1 = new javax.swing.JLayeredPane();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblPatientDirectory = new javax.swing.JTable();
        btnViewPatientDetail = new javax.swing.JButton();
        btnAddNewPatient = new javax.swing.JButton();
        btnSearch = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();

        javax.swing.GroupLayout jLayeredPane1Layout = new javax.swing.GroupLayout(jLayeredPane1);
        jLayeredPane1.setLayout(jLayeredPane1Layout);
        jLayeredPane1Layout.setHorizontalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jLayeredPane1Layout.setVerticalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("Manage Patient Directory");

        tblPatientDirectory.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        tblPatientDirectory.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name", "Age", "Mobile Number", "Email Id", "Primary Doctor Name", "Preferred Pharmacy"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblPatientDirectory);

        btnViewPatientDetail.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnViewPatientDetail.setText("View Patient Detail >>");
        btnViewPatientDetail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewPatientDetailActionPerformed(evt);
            }
        });

        btnAddNewPatient.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnAddNewPatient.setText("Add New Patient >>");
        btnAddNewPatient.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddNewPatientActionPerformed(evt);
            }
        });

        btnSearch.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnSearch.setText("Search >>");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        btnBack.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnBack.setText("<< Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnAddNewPatient)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(420, 420, 420)
                            .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(20, 20, 20)
                            .addComponent(jLabel1)))
                    .addComponent(btnViewPatientDetail, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 521, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(108, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1)
                .addGap(19, 19, 19)
                .addComponent(btnSearch)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnViewPatientDetail)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnAddNewPatient)
                .addGap(18, 18, 18)
                .addComponent(btnBack)
                .addContainerGap(38, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    private void btnViewPatientDetailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewPatientDetailActionPerformed
        int selectedRow = tblPatientDirectory.getSelectedRow();
        if(selectedRow<0)
        {  JOptionPane.showMessageDialog(null, "please select a person from the table ");
            return;
        }
        Patient patient = (Patient) tblPatientDirectory.getValueAt(selectedRow, 0);
        
        ViewPatientDetailJPanel viewPatientDetailJPanel = new ViewPatientDetailJPanel( patient , userProcessContainer);
        userProcessContainer.add("ViewPatientDetail",viewPatientDetailJPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
        
        
    }//GEN-LAST:event_btnViewPatientDetailActionPerformed

    private void btnAddNewPatientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddNewPatientActionPerformed
        
        SearchForPersonCreatePatientJPanel searchPatient1 = new SearchForPersonCreatePatientJPanel( patientDirectory, personDirectory, userProcessContainer );
        userProcessContainer.add("searchpatient1",searchPatient1);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnAddNewPatientActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
    
        SearchForPatientJPanel searchForPatientJPanel = new SearchForPatientJPanel( patientDirectory, userProcessContainer);
        userProcessContainer.add("searchForPatient",searchForPatientJPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnSearchActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
            userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddNewPatient;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnSearch;
    private javax.swing.JButton btnViewPatientDetail;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLayeredPane jLayeredPane1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblPatientDirectory;
    // End of variables declaration//GEN-END:variables
}
