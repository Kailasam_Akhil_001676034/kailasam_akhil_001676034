package UserInterface.PersonRole;

import Business.PatientDirectory;
import Business.Person;
import Business.PersonDirectory;

import java.awt.CardLayout;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;


/**
 *
 * @author Rushabh
 */
public class ManagePersonDirectoryJPanel extends javax.swing.JPanel {

    
  
    /** Creates new form ManagePersonDirectoryJPanel */
    private PatientDirectory patientDirectory;
    private JPanel userProcessContainer;
    private PersonDirectory personDirectory;
    
    public ManagePersonDirectoryJPanel(PatientDirectory patientDirectory, PersonDirectory personDirectory, JPanel userProcessContainer) {
        initComponents();
        this.personDirectory = personDirectory;
        this.userProcessContainer = userProcessContainer;
        this.patientDirectory=patientDirectory;
        refreshTable();
    
    }
private void refreshTable()
    {  DefaultTableModel dtm = (DefaultTableModel) tblPersonDirectory.getModel();
        dtm.setRowCount(0);
        
        for(Person p : personDirectory.getPersonList())
        {  Object[] row = new Object[6];
            row[0]=p;
            row[1]=p.getAge();
            row[2]=p.getMobilenumber();
            row[3]=p.getEmail();
            row[4]=p.getPrimarydoctorname();
            row[5]=p.getPreferredpharmacy();
            dtm.addRow(row);
            
            
            
        }
    }
    
    
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLayeredPane1 = new javax.swing.JLayeredPane();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblPersonDirectory = new javax.swing.JTable();
        btnViewPersonDetail = new javax.swing.JButton();
        btnAddNewPerson = new javax.swing.JButton();
        btnSearch = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();

        javax.swing.GroupLayout jLayeredPane1Layout = new javax.swing.GroupLayout(jLayeredPane1);
        jLayeredPane1.setLayout(jLayeredPane1Layout);
        jLayeredPane1Layout.setHorizontalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jLayeredPane1Layout.setVerticalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("Manage Person Directory");

        tblPersonDirectory.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        tblPersonDirectory.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name", "Age", "Mobile Number", "Email Id", "Primary Doctor Name", "Preferred Pharmacy"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblPersonDirectory);

        btnViewPersonDetail.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnViewPersonDetail.setText("View Person Detail >>");
        btnViewPersonDetail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewPersonDetailActionPerformed(evt);
            }
        });

        btnAddNewPerson.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnAddNewPerson.setText("Add New Person >>");
        btnAddNewPerson.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddNewPersonActionPerformed(evt);
            }
        });

        btnSearch.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnSearch.setText("Search >>");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        btnBack.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnBack.setText("<< Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnAddNewPerson)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(420, 420, 420)
                            .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(20, 20, 20)
                            .addComponent(jLabel1)))
                    .addComponent(btnViewPersonDetail, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 521, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(108, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1)
                .addGap(19, 19, 19)
                .addComponent(btnSearch)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnViewPersonDetail)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnAddNewPerson)
                .addGap(18, 18, 18)
                .addComponent(btnBack)
                .addContainerGap(38, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    private void btnViewPersonDetailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewPersonDetailActionPerformed
        int selectedRow = tblPersonDirectory.getSelectedRow();
        if(selectedRow<0)
        {  JOptionPane.showMessageDialog(null, "please select a person from the table ");
            return;
        }
        Person person = (Person) tblPersonDirectory.getValueAt(selectedRow, 0);
        
        ViewPersonDetailJPanel viewPersonDetailJPanel = new ViewPersonDetailJPanel( person , patientDirectory, personDirectory, userProcessContainer);
        userProcessContainer.add("ViewPersonDetail",viewPersonDetailJPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
        
        
    }//GEN-LAST:event_btnViewPersonDetailActionPerformed

    private void btnAddNewPersonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddNewPersonActionPerformed
        
        AddNewPersonJPanel addNewPersonJPanel = new AddNewPersonJPanel( personDirectory, userProcessContainer );
        userProcessContainer.add("AddNewPerson",addNewPersonJPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnAddNewPersonActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
    
        SearchForPersonJPanel searchForPersonJPanel = new SearchForPersonJPanel( patientDirectory, personDirectory, userProcessContainer);
        userProcessContainer.add("SearchForPerson",searchForPersonJPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnSearchActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
            userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddNewPerson;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnSearch;
    private javax.swing.JButton btnViewPersonDetail;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLayeredPane jLayeredPane1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblPersonDirectory;
    // End of variables declaration//GEN-END:variables
}
