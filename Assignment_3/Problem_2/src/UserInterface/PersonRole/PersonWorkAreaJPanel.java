package UserInterface.PersonRole;

import Business.PatientDirectory;
import Business.PersonDirectory;
import java.awt.CardLayout;
import javax.swing.JPanel;

/**
 *
 * @author Mihir Mehta / Hechen Gao
 */
public class PersonWorkAreaJPanel extends javax.swing.JPanel {
 private PatientDirectory patientDirectory;
    private JPanel userProcessContainer;
    private PersonDirectory personDirectory;
    
    public PersonWorkAreaJPanel(PatientDirectory patientDirectory, PersonDirectory personDirectory, JPanel userProcessContainer) {
        initComponents();
        this.personDirectory = personDirectory;
        this.userProcessContainer = userProcessContainer;
        this.patientDirectory=patientDirectory;
    
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        btnManagePersonDirectory = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("Person Profile Manager");

        btnManagePersonDirectory.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnManagePersonDirectory.setText("Manage Person Directory >>");
        btnManagePersonDirectory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnManagePersonDirectoryActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(130, 130, 130)
                        .addComponent(btnManagePersonDirectory))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(111, 111, 111)
                        .addComponent(jLabel1)))
                .addContainerGap(162, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(jLabel1)
                .addGap(86, 86, 86)
                .addComponent(btnManagePersonDirectory)
                .addContainerGap(87, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnManagePersonDirectoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnManagePersonDirectoryActionPerformed
        // TODO add your handling code here:
        ManagePersonDirectoryJPanel managePersonDirectoryJPanel = new ManagePersonDirectoryJPanel( patientDirectory,personDirectory, userProcessContainer);
        userProcessContainer.add("ManagePersonDirectory",managePersonDirectoryJPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
        
    }//GEN-LAST:event_btnManagePersonDirectoryActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnManagePersonDirectory;
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables
}
