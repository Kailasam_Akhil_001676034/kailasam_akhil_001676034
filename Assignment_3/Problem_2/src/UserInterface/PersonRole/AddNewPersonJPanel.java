package UserInterface.PersonRole;

import Business.Person;
import Business.PersonDirectory;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;


/**
 *
 * @author Mihir Mehta / Hechen Gao
 */
public class AddNewPersonJPanel extends javax.swing.JPanel {
private PersonDirectory personDirectory;
    private JPanel userProcessContainer;
    
    public AddNewPersonJPanel(PersonDirectory personDirectory, JPanel userProcessContainer) {
        initComponents();
        this.personDirectory = personDirectory;
        this.userProcessContainer = userProcessContainer;
        
        
    
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnBack = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        txtAge = new javax.swing.JTextField();
        txtMobileNumber = new javax.swing.JTextField();
        txtPrimaryDoctorName = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        txtPreferredPharmacy = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        btnSubmit = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtEmailId = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();

        btnBack.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnBack.setText("<< Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("Preferred Pharmacy");
        jLabel7.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        txtName.setFont(new java.awt.Font("Calibri", 0, 11)); // NOI18N
        txtName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNameActionPerformed(evt);
            }
        });

        txtAge.setFont(new java.awt.Font("Calibri", 0, 11)); // NOI18N
        txtAge.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAgeActionPerformed(evt);
            }
        });

        txtMobileNumber.setFont(new java.awt.Font("Calibri", 0, 11)); // NOI18N
        txtMobileNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMobileNumberActionPerformed(evt);
            }
        });

        txtPrimaryDoctorName.setFont(new java.awt.Font("Calibri", 0, 11)); // NOI18N
        txtPrimaryDoctorName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPrimaryDoctorNameActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel1.setText("Person Details");

        txtPreferredPharmacy.setFont(new java.awt.Font("Calibri", 0, 11)); // NOI18N
        txtPreferredPharmacy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPreferredPharmacyActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jLabel2.setText("Please enter below details of Person. All fields are mandatory");

        btnSubmit.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        btnSubmit.setText("Submit");
        btnSubmit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSubmitActionPerformed(evt);
            }
        });

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Name");
        jLabel3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Age");
        jLabel4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Mobile Number");
        jLabel5.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Primary Doctor Name");
        jLabel6.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        txtEmailId.setFont(new java.awt.Font("Calibri", 0, 11)); // NOI18N
        txtEmailId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtEmailIdActionPerformed(evt);
            }
        });

        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Email Id");
        jLabel8.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addComponent(btnBack))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(80, 80, 80)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(txtAge, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(txtMobileNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(txtPrimaryDoctorName, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(btnSubmit)
                                        .addComponent(txtPreferredPharmacy, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(143, 143, 143)
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(txtEmailId, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(87, 87, 87))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAge, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtMobileNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtEmailId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPrimaryDoctorName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPreferredPharmacy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(32, 32, 32)
                .addComponent(btnSubmit)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnBack))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
            userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
        
    }//GEN-LAST:event_btnBackActionPerformed

    private void txtNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNameActionPerformed

    private void txtAgeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAgeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtAgeActionPerformed

    private void txtMobileNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMobileNumberActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMobileNumberActionPerformed

    private void txtPrimaryDoctorNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPrimaryDoctorNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPrimaryDoctorNameActionPerformed

    private void txtPreferredPharmacyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPreferredPharmacyActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPreferredPharmacyActionPerformed

    private void btnSubmitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSubmitActionPerformed
        // TODO add your handling code here:
        String N;
        int A;
        int MN;
        String EID;
        String PDN;
        String PP;
        
        if (txtName.getText().isEmpty()) {
            JOptionPane.showMessageDialog(txtName, "Please enter input for the field - Name");
            return;
        } else {

            String a = txtName.getText();
            if (!a.matches("[ a-zA-Z_]+")) {
                JOptionPane.showMessageDialog(txtName, "Invalid input for the field - Name\nplease enter input data only from below\n a-z\n A-Z");
                return;
            } else {
                N=txtName.getText().trim();
            }
        }


        if (txtAge.getText().isEmpty()) {
            JOptionPane.showMessageDialog(txtAge, "Please enter input for the field - Age ");
            return;
        } else {
            String str = txtAge.getText().trim();
            try {
                double isNum = Double.parseDouble(str);
                if (isNum == Math.floor(isNum) && isNum > 0 && isNum<150) {

                    A=Integer.parseInt(txtAge.getText().trim());

                } else {
                    JOptionPane.showMessageDialog(txtAge, "Invalid input for the field - Age\nAge cannot be more than 150\nplease enter input data using below digits only\n0-9");
                    return;
                }
            } catch (Exception e) {
                if (str.toCharArray().length == 1) {
                    JOptionPane.showMessageDialog(txtAge, "Invalid input for the field - Age\nplease enter input data using below digits only\n0-9");
                    return;
                } else {
                    JOptionPane.showMessageDialog(txtAge, "Invalid input for the field - Age\nplease enter input data using below digits only\n0-9");
                    return;
                }
            }

        }
        
         if (txtMobileNumber.getText().isEmpty()) {
            JOptionPane.showMessageDialog(txtMobileNumber, "Please enter input for the field - Mobile Number ");
            return;
        } else {
            String str = txtMobileNumber.getText().trim();
            try {
                double isNum = Double.parseDouble(str);
                if (isNum == Math.floor(isNum) && isNum > 0 && str.length() == 10) {

                    MN=Integer.parseInt(txtMobileNumber.getText().trim());

                } else {
                    JOptionPane.showMessageDialog(txtMobileNumber, "Please enter only 10 digit number as input for the field - Mobile Number");
                    return;
                }
            } catch (Exception e) {
                if (str.toCharArray().length == 1) {
                    JOptionPane.showMessageDialog(txtMobileNumber, "Please enter only 10 digit number as input for the field - Mobile Number ");
                    return;
                } else {
                    JOptionPane.showMessageDialog(txtMobileNumber, "Please enter only 10 digit number as input for the field - Mobile Number ");
                    return;
                }
            }

        }

        
        
        
        
        
                if (txtEmailId.getText().isEmpty()) {
            JOptionPane.showMessageDialog(txtEmailId, "Please enter input for the field - Email Id");
            return;
        } else {

            String a = txtEmailId.getText();
            if (!a.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
                JOptionPane.showMessageDialog(txtEmailId, "Invalid input for the field - Email Id. please enter input data only from below\n a-z\n A-Z\n 0-9\n @.+-\n sample email addresses as follows: "
                        + "\n mkyong@yahoo.com\n mkyong-100@yahoo.com\n mkyong.100@yahoo.com\n"
                        + " mkyong111@mkyong.com\n mkyong-100@mkyong.net");
                return;
            } else {
                EID= txtEmailId.getText().trim();
            }
        }


        if (txtPrimaryDoctorName.getText().isEmpty()) {
            JOptionPane.showMessageDialog(txtPrimaryDoctorName, "Please enter input for the field - Primary Doctor Name");
            return;
        } else {

            String a = txtPrimaryDoctorName.getText();
            if (!a.matches("[ a-zA-Z_]+")) {
                JOptionPane.showMessageDialog(txtPrimaryDoctorName, "Invalid input for the field - Primary Doctor Name. please enter input data only from below\n a-z\n A-Z");
                return;
            } else {
                PDN=txtPrimaryDoctorName.getText().trim();
            }
        }

        if (txtPreferredPharmacy.getText().isEmpty()) {
            JOptionPane.showMessageDialog(txtPreferredPharmacy, "Please enter input for the field - Preferred Pharmacy");
            return;
        } else {

            String a = txtPreferredPharmacy.getText();
            if (!a.matches("[ a-zA-Z_]+")) {
                JOptionPane.showMessageDialog(txtPreferredPharmacy, "Invalid input for the field - Preferred Pharmacy. please enter input data only from below\n a-z\n A-Z");
                return;
            } else {
                PP=txtPreferredPharmacy.getText().trim();
            }
        }
        
        int mn= Integer.parseInt(txtAge.getText().trim());
        Person p = personDirectory.searchPerson(mn);
        if(p!=null)
        {
            JOptionPane.showMessageDialog(null, "Person with these details is found in database. To add person, please recheck the details \nor \nTo view person details, go to Person-->View Person Directory-->Search");
               return;
               
        }
        
        else{
        Person person = personDirectory.addPerson();
        person.setName(N);
        person.setAge(A);
        person.setMobilenumber(MN);
        person.setEmail(EID);
        person.setPrimarydoctorname(PDN);
        person.setPreferredpharmacy(PP);
        
        
        JOptionPane.showMessageDialog(null, "Person added successfully");

        txtName.setText("");
        txtAge.setText("");
        txtMobileNumber.setText("");
        txtEmailId.setText("");
        txtPrimaryDoctorName.setText("");
        txtPreferredPharmacy.setText("");
        }
    }//GEN-LAST:event_btnSubmitActionPerformed

    private void txtEmailIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtEmailIdActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtEmailIdActionPerformed
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnSubmit;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JTextField txtAge;
    private javax.swing.JTextField txtEmailId;
    private javax.swing.JTextField txtMobileNumber;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtPreferredPharmacy;
    private javax.swing.JTextField txtPrimaryDoctorName;
    // End of variables declaration//GEN-END:variables
}
