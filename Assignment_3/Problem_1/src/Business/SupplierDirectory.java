/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author akki
 */
public class SupplierDirectory {
    
    private ArrayList<Supplier> supplierDirectory;
    
    public SupplierDirectory()
    {
        supplierDirectory = new ArrayList<Supplier>();
    
    
    }

    public ArrayList<Supplier> getSupplierDirectory() {
        return supplierDirectory;
    }

  public Supplier addSupplier(String str) {
        Supplier p = new Supplier();
        p.setSupplierName(str);
        supplierDirectory.add(p);
        return p;
    
    }

    
    
    public void deleteSupplier(Supplier p)
    {
    supplierDirectory.remove(p);
    
    }
    
    public Supplier searchSupplier(String keyWord)
    {
    for(Supplier s: supplierDirectory)
    {
    if(s.getSupplierName().equalsIgnoreCase(keyWord))
    { return s;   
    }
    
    
    }
    return null;
}
}
