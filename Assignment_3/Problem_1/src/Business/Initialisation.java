/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author akki
 */
public class Initialisation {

    public Initialisation(SupplierDirectory supplierDirectory, ProductCatalog productCatalog) {

        Supplier dell = supplierDirectory.addSupplier("DELL");

        Supplier hp = supplierDirectory.addSupplier("HP");
        Supplier toshiba = supplierDirectory.addSupplier("TOSHIBA");
        Supplier apple = supplierDirectory.addSupplier("APPLE");
        Supplier lenovo = supplierDirectory.addSupplier("LENOVO");

        ProductCatalog dellcatalog = dell.getProductCatalog();

        Product d1 = dellcatalog.addProduct("laptop1", "100");
        Product d2 = dellcatalog.addProduct("laptop2", "200");
        Product d3 = dellcatalog.addProduct("laptop3", "300");
        Product d4 = dellcatalog.addProduct("laptop4", "400");
        Product d5 = dellcatalog.addProduct("laptop5", "500");
        Product d6 = dellcatalog.addProduct("laptop6", "600");
        Product d7 = dellcatalog.addProduct("laptop7", "700");
        Product d8 = dellcatalog.addProduct("laptop8", "800");
        Product d9 = dellcatalog.addProduct("laptop9", "900");
        Product d10 = dellcatalog.addProduct("laptop10", "1000");

        ProductCatalog hpcatalog = hp.getProductCatalog();
        ProductCatalog toshibacatalog = toshiba.getProductCatalog();
        ProductCatalog applecatalog = apple.getProductCatalog();
        ProductCatalog lenovocatalog = lenovo.getProductCatalog();

        Product h1 = hpcatalog.addProduct("laptop1", "100");
        Product h2 = hpcatalog.addProduct("laptop2", "200");
        Product h3 = hpcatalog.addProduct("laptop3", "300");
        Product h4 = hpcatalog.addProduct("laptop4", "400");
        Product h5 = hpcatalog.addProduct("laptop5", "500");
        Product h6 = hpcatalog.addProduct("laptop6", "600");
        Product h7 = hpcatalog.addProduct("laptop7", "700");
        Product h8 = hpcatalog.addProduct("laptop8", "800");
        Product h9 = hpcatalog.addProduct("laptop9", "900");
        Product h10 = hpcatalog.addProduct("laptop10", "1000");

        Product t1 = toshibacatalog.addProduct("laptop1", "100");
        Product t2 = toshibacatalog.addProduct("laptop2", "200");
        Product t3 = toshibacatalog.addProduct("laptop3", "300");
        Product t4 = toshibacatalog.addProduct("laptop4", "400");
        Product t5 = toshibacatalog.addProduct("laptop5", "500");
        Product t6 = toshibacatalog.addProduct("laptop6", "600");
        Product t7 = toshibacatalog.addProduct("laptop7", "700");
        Product t8 = toshibacatalog.addProduct("laptop8", "800");
        Product t9 = toshibacatalog.addProduct("laptop9", "900");
        Product t10 = toshibacatalog.addProduct("laptop10", "1000");

        Product a1 = applecatalog.addProduct("laptop1", "100");
        Product a2 = applecatalog.addProduct("laptop2", "200");
        Product a3 = applecatalog.addProduct("laptop3", "300");
        Product a4 = applecatalog.addProduct("laptop4", "400");
        Product a5 = applecatalog.addProduct("laptop5", "500");
        Product a6 = applecatalog.addProduct("laptop6", "600");
        Product a7 = applecatalog.addProduct("laptop7", "700");
        Product a8 = applecatalog.addProduct("laptop8", "800");
        Product a9 = applecatalog.addProduct("laptop9", "900");
        Product a10 = applecatalog.addProduct("laptop10", "1000");

        Product l1 = lenovocatalog.addProduct("laptop1", "100");
        Product l2 = lenovocatalog.addProduct("laptop2", "200");
        Product l3 = lenovocatalog.addProduct("laptop3", "300");
        Product l4 = lenovocatalog.addProduct("laptop4", "400");
        Product l5 = lenovocatalog.addProduct("laptop5", "500");
        Product l6 = lenovocatalog.addProduct("laptop6", "600");
        Product l7 = lenovocatalog.addProduct("laptop7", "700");
        Product l8 = lenovocatalog.addProduct("laptop8", "800");
        Product l9 = lenovocatalog.addProduct("laptop9", "900");
        Product l10 = lenovocatalog.addProduct("laptop10", "1000");

    }

    public void Print(SupplierDirectory supplierDirectory, ProductCatalog productCatalog) {
        for (Supplier s : supplierDirectory.getSupplierDirectory()) {
            System.out.println("Supplier name:" + s.getSupplierName());
            System.out.println(s.getSupplierName() + " Product details");
            for (Product p : s.getProductCatalog().getProductList()) {
                //String ID=String.valueOf(p.getId());

                System.out.println("" + p.getId() + "\t" + p.getProductName() + "\t" + p.getPrice());

            }

        }

    }

    public static void main(String args[]) {
        SupplierDirectory supplierDirectory = new SupplierDirectory();
        ProductCatalog productCatalog = new ProductCatalog();
        Initialisation i = new Initialisation(supplierDirectory, productCatalog);
        i.Print(supplierDirectory, productCatalog);
    }
}
