/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author akki
 */
public class InventoryItem {
    
    private Product product;
    private int threshold;
    private int salesPrice;
    private int iAvailableQuantity;
    private int margin;
    
    public InventoryItem(){
        
        threshold = 5;
        //availableQuantity=0;
    }
    
    @Override
    public String toString() {
        return product.getpName();
    }

    
    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getThreshold() {
        return threshold;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }

    public int getSalesPrice() {
        return salesPrice;
    }

    public void setSalesPrice(int salesPrice) {
        this.salesPrice = salesPrice;
    }

    public int getiAvailableQuantity() {
        return iAvailableQuantity;
    }

    public void setiAvailableQuantity(int iAvailableQuantity) {
        this.iAvailableQuantity = iAvailableQuantity;
    }

    public int getMargin() {
        return margin;
    }

    public void setMargin(int margin) {
        this.margin = margin;
    }

    

    
    
    
    
}
