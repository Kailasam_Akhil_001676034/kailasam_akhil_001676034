/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author akki
 */
public class Product {
    
    private String pName;
    private int pID;
    private String pCategory;
    private String pBrand;
    private int pWeight;
    private int pListedPrice;
    private int pAvailableQuantity;
    
    private String pLicenseNumber;
    private int pCustomerDemand;
    private int pCustomerSatisfaction;
    
    private static int count =0;
    
    public Product(){
        count++;
        pID=count;
    }

    @Override
    public String toString() {
        return pName; 
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public int getpID() {
        return pID;
    }

    public void setpID(int pID) {
        this.pID = pID;
    }

    public String getpCategory() {
        return pCategory;
    }

    public void setpCategory(String pCategory) {
        this.pCategory = pCategory;
    }

    public String getpBrand() {
        return pBrand;
    }

    public void setpBrand(String pBrand) {
        this.pBrand = pBrand;
    }

    public int getpWeight() {
        return pWeight;
    }

    public void setpWeight(int pWeight) {
        this.pWeight = pWeight;
    }

    public int getpListedPrice() {
        return pListedPrice;
    }

    public void setpListedPrice(int pListedPrice) {
        this.pListedPrice = pListedPrice;
    }

    public int getpAvailableQuantity() {
        return pAvailableQuantity;
    }

    public void setpAvailableQuantity(int pAvailableQuantity) {
        this.pAvailableQuantity = pAvailableQuantity;
    }


    public int getpCustomerDemand() {
        return pCustomerDemand;
    }

    public void setpCustomerDemand(int pCustomerDemand) {
        this.pCustomerDemand = pCustomerDemand;
    }

    public int getpCustomerSatisfaction() {
        return pCustomerSatisfaction;
    }

    public void setpCustomerSatisfaction(int pCustomerSatisfaction) {
        this.pCustomerSatisfaction = pCustomerSatisfaction;
    }

    public String getpLicenseNumber() {
        return pLicenseNumber;
    }

    public void setpLicenseNumber(String pLicenseNumber) {
        this.pLicenseNumber = pLicenseNumber;
    }
    
    
    
}
