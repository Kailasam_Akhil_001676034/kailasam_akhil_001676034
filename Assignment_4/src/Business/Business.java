/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author akki
 */
public class Business {
    
    private StoreDirectory storeDirectory;
    private ProductCatalog productCatalog;
    
    public Business() {
        
        storeDirectory = new StoreDirectory();
        productCatalog = new ProductCatalog();
    }

    public StoreDirectory getStoreDirectory() {
        return storeDirectory;
    }

    

    public ProductCatalog getProductCatalog() {
        return productCatalog;
    }

    
}
