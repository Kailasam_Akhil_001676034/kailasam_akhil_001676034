/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author akki
 */
public class Store {
    
    private InventoryItemCatalog inventoryItemCatalog;
    
    private String sLocation;
//    private String sStoreManager;
//    private String sContactNumber;
//    private String sEmail;
    
    public Store(){
        inventoryItemCatalog = new InventoryItemCatalog();
    }
    
    @Override
    public String toString() {
        return sLocation; 
    }

    public InventoryItemCatalog getInventoryProductCatalog() {
        return inventoryItemCatalog;
    }

    public void setInventoryProductCatalog(InventoryItemCatalog inventoryProductCatalog) {
        this.inventoryItemCatalog = inventoryProductCatalog;
    }

    public String getsLocation() {
        return sLocation;
    }

    public void setsLocation(String sLocation) {
        this.sLocation = sLocation;
    }
    
    
    
    
    
}
