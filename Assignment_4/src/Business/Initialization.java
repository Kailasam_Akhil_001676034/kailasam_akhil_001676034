/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.function.Supplier;

/**
 *
 * @author akki
 */
public class Initialization {
    public Initialization(StoreDirectory storeDirectory, ProductCatalog productCatalog) {

        Product p1 = productCatalog.addProduct();
        p1.setpName("Milk");
        p1.setpCategory("Food");
        p1.setpBrand("Hood");
        p1.setpLicenseNumber("AK113");
        
        p1.setpID(1);
        p1.setpAvailableQuantity(6);
        p1.setpWeight(100);
        p1.setpListedPrice(100);
        p1.setpCustomerDemand(10);
        p1.setpCustomerSatisfaction(10);
        
        Store boston = storeDirectory.addStore();
        boston.setsLocation("Boston");
        
        InventoryItem i = boston.getInventoryProductCatalog().addInventoryItem();
        i.setProduct(p1);
        i.setSalesPrice(1000);
        i.setiAvailableQuantity(4);
        i.setMargin(i.getSalesPrice() - i.getProduct().getpListedPrice());
        
         
    }
    

      
    
}
