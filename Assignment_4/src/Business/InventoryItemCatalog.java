/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author akki
 */
public class InventoryItemCatalog {
    
    private ArrayList<InventoryItem> inventoryItemList;
    
    
    public InventoryItemCatalog() {
        
        inventoryItemList = new  ArrayList<InventoryItem>();
    }
    
    public ArrayList<InventoryItem> getInventoryItemList() {
        return inventoryItemList;
    }
    
    public void removeInventoryItem(InventoryItem i) {
        inventoryItemList.remove(i);
    }
    
    public InventoryItem addInventoryItem() {
        InventoryItem i = new InventoryItem();
        
        inventoryItemList.add(i);
        return i;
    }   
    
    public InventoryItem addInventoryItem(Product p, int q, int listedprice, int salesprice) {
        InventoryItem i = new InventoryItem();
        i.setProduct(p);
        i.setiAvailableQuantity(q);
        i.setSalesPrice(salesprice);
        int x=salesprice-listedprice;
        i.setMargin(x);
        inventoryItemList.add(i);
        return i;
    }   

    public InventoryItem searchInventoryItem(String name){
        for (InventoryItem i : inventoryItemList) {
            if(i.getProduct().getpName().equals(name)){
                return i;
            }
        }
        return null;
    }

    

   

    
}
