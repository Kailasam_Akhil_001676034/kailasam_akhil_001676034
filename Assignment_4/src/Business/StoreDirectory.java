/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author akki
 */
public class StoreDirectory {
    
    private ArrayList<Store> storeList;

    public StoreDirectory() {
    storeList = new ArrayList<Store>();
    }
    
    public ArrayList<Store> getStoreList() {
        return storeList;
    }

    
    public Store addStore(){
        Store p = new Store();
        storeList.add(p);
        return p;
    }
    
    public void removeStore(Store p){
        storeList.remove(p);
    }
    
    public Store searchStore(String loc){
        for (Store s : storeList) {
            if(s.getsLocation().equals(loc)){
                return s;
            }
        }
        return null;
    }

    
}
