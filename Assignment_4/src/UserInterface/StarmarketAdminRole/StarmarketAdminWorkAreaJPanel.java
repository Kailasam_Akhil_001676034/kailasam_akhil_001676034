/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.StarmarketAdminRole;

import Business.ProductCatalog;
import Business.StoreDirectory;
import java.awt.CardLayout;
import javax.swing.JPanel;

/**
 *
 * @author akki
 */
public class StarmarketAdminWorkAreaJPanel extends javax.swing.JPanel {

    /**
     * Creates new form StarmarketAdminWorkAreaJPanel
     */
    
    private StoreDirectory storeDirectory;
    private ProductCatalog productCatalog;
    private JPanel userProcessContainer;
    public StarmarketAdminWorkAreaJPanel(JPanel userProcessContainer,ProductCatalog productCatalog, StoreDirectory storeDirectory  ) {
        initComponents();
        this.userProcessContainer=userProcessContainer;
        this.storeDirectory=storeDirectory;
        this.productCatalog=productCatalog;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnManageProductCatalog = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        btnManageStores = new javax.swing.JButton();

        btnManageProductCatalog.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnManageProductCatalog.setText("Manage Product Catalog >>");
        btnManageProductCatalog.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnManageProductCatalogActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("My Work Area - Starmarket Admin Role");

        btnManageStores.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnManageStores.setText("Manage Stores >>");
        btnManageStores.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnManageStoresActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(117, 117, 117)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(70, 70, 70)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnManageStores, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnManageProductCatalog, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(47, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addComponent(jLabel1)
                .addGap(81, 81, 81)
                .addComponent(btnManageProductCatalog)
                .addGap(18, 18, 18)
                .addComponent(btnManageStores)
                .addContainerGap(200, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnManageProductCatalogActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnManageProductCatalogActionPerformed
        ManageProductCatalogJPanel ManageProductCatalogJPanel = new ManageProductCatalogJPanel(userProcessContainer,productCatalog);
        userProcessContainer.add("ManageProductCatalogJPanel", ManageProductCatalogJPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnManageProductCatalogActionPerformed

    private void btnManageStoresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnManageStoresActionPerformed
        // TODO add your handling code here:
        ManageStoreDirectoryJPanel ManageStoreDirectoryJPanel = new ManageStoreDirectoryJPanel(userProcessContainer, productCatalog,  storeDirectory);
        userProcessContainer.add("ManageStoreDirectoryJPanel", ManageStoreDirectoryJPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnManageStoresActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnManageProductCatalog;
    private javax.swing.JButton btnManageStores;
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables
}
