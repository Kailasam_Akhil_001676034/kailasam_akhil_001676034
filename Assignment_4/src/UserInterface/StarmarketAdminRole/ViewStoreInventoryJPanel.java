/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.StarmarketAdminRole;

import Business.InventoryItem;
import Business.Product;
import Business.ProductCatalog;
import Business.Store;
import Business.StoreDirectory;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author akki
 */
public class ViewStoreInventoryJPanel extends javax.swing.JPanel {

    /**
     * Creates new form ViewStoreInventoryJPanel
     */
        private StoreDirectory storeDirectory;
    private ProductCatalog productCatalog;
    private Store store;
    private JPanel userProcessContainer;
    
    
    public ViewStoreInventoryJPanel(JPanel userProcessContainer,ProductCatalog productCatalog, StoreDirectory storeDirectory, Store store ) {
        initComponents();
        this.userProcessContainer=userProcessContainer;
        this.storeDirectory=storeDirectory;
        this.productCatalog=productCatalog;
        this.store= store;
        
        lblloc.setText(store.getsLocation());
        populateInventoryTable();
        populateProductTable();
        
    }

    private void populateInventoryTable() {
        DefaultTableModel dtm1 = (DefaultTableModel) tblInventoryItemCatalog.getModel();
        
        dtm1.setRowCount(0);
        
            for (InventoryItem  i : store.getInventoryProductCatalog().getInventoryItemList()) {
                Object row[] = new Object[10];
                row[0] = i;
                row[1] = i.getProduct().getpID();
                row[2] = i.getProduct().getpCategory();
                row[3] = i.getProduct().getpBrand();
                row[4] = i.getProduct().getpWeight();
                row[5] = i.getSalesPrice();
                row[6] = i.getiAvailableQuantity();
                row[7] = i.getProduct().getpLicenseNumber();
                row[8] = i.getProduct().getpCustomerDemand();
                row[9] = i.getProduct().getpCustomerSatisfaction();    
                
                dtm1.addRow(row);
            }
        
    }
    
    private void refreshInventoryTable(String keyWord) {
        int rowCount = tblInventoryItemCatalog.getRowCount();
        DefaultTableModel model1 = (DefaultTableModel) tblInventoryItemCatalog.getModel();
        for (int i = rowCount - 1; i >= 0; i--) {
            model1.removeRow(i);
        }

        
            for (InventoryItem  i : store.getInventoryProductCatalog().getInventoryItemList()) {
                if (keyWord.equals(i.getProduct().getpName())) {
                Object row[] = new Object[10];
                row[0] = i;
                row[1] = i.getProduct().getpID();
                row[2] = i.getProduct().getpCategory();
                row[3] = i.getProduct().getpBrand();
                row[4] = i.getProduct().getpWeight();
                row[5] = i.getSalesPrice();
                row[6] = i.getiAvailableQuantity();
                row[7] = i.getProduct().getpLicenseNumber();
                row[8] = i.getProduct().getpCustomerDemand();
                row[9] = i.getProduct().getpCustomerSatisfaction();       

                    model1.addRow(row);
                }
            }
            if(tblInventoryItemCatalog.getRowCount()==0){
                JOptionPane.showMessageDialog(null, "No matches found", "INFO", JOptionPane.INFORMATION_MESSAGE);
                populateInventoryTable();
                txtSearchKeyWord.setText("");
            }
        

    }
    
    private void populateProductTable() {
        DefaultTableModel dtm = (DefaultTableModel) tblProductCatalog.getModel();
        
        dtm.setRowCount(0);
        
            for (Product product : productCatalog.getProductList()) {
                Object row[] = new Object[10];
                row[0] = product;
                row[1] = product.getpID();
                row[2] = product.getpCategory();
                row[3] = product.getpBrand();
                row[4] = product.getpWeight();
                row[5] = product.getpListedPrice();
                row[6] = product.getpAvailableQuantity();
                row[7] = product.getpLicenseNumber();
                row[8] = product.getpCustomerDemand();
                row[9] = product.getpCustomerSatisfaction();    
                
                dtm.addRow(row);
            }
        
    }
    
    private void refreshProductTable(String keyWord) {
        int rowCount = tblProductCatalog.getRowCount();
        DefaultTableModel model = (DefaultTableModel) tblProductCatalog.getModel();
        for (int i = rowCount - 1; i >= 0; i--) {
            model.removeRow(i);
        }

        
            for (Product product : productCatalog.getProductList()) {
                if (keyWord.equals(product.getpName())) {
                Object row[] = new Object[10];
                row[0] = product;
                row[1] = product.getpID();
                row[2] = product.getpCategory();
                row[3] = product.getpBrand();
                row[4] = product.getpWeight();
                row[5] = product.getpListedPrice();
                row[6] = product.getpAvailableQuantity();
                row[7] = product.getpLicenseNumber();
                row[8] = product.getpCustomerDemand();
                row[9] = product.getpCustomerSatisfaction();    

                    model.addRow(row);
                }
            }
            if(tblProductCatalog.getRowCount()==0){
                JOptionPane.showMessageDialog(null, "No matches found", "INFO", JOptionPane.INFORMATION_MESSAGE);
                populateProductTable();
                txtSearchKeyWord.setText("");
            }
        

    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        tblProductCatalog = new javax.swing.JTable();
        txtSearchKeyWord = new javax.swing.JTextField();
        btnSearchProduct = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblInventoryItemCatalog = new javax.swing.JTable();
        btnSearchProduct1 = new javax.swing.JButton();
        txtSearchKeyWord1 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        lblloc = new javax.swing.JLabel();
        backButton1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        tblProductCatalog.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Product Name", "Product ID", "Category", "Brand", "Weight", "Listed Price", "Available Quantity", "License Number", "Customer Demand", "Customer Satisfaction"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(tblProductCatalog);

        btnSearchProduct.setText("Search Product By Name");
        btnSearchProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchProductActionPerformed(evt);
            }
        });

        tblInventoryItemCatalog.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Product Name", "Product ID", "Category", "Brand", "Weight", "Sales Price", "Available Quantity", "License Number", "Customer Demand", "Customer Satisfaction"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(tblInventoryItemCatalog);

        btnSearchProduct1.setText("Search Product By Name");
        btnSearchProduct1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchProduct1ActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("Store:");

        lblloc.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        lblloc.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        backButton1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        backButton1.setText("<< Back");
        backButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButton1ActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Store Inventory:");

        jLabel3.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Product Catalog:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(txtSearchKeyWord, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnSearchProduct)
                .addGap(153, 153, 153))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(backButton1))
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 795, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 795, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(296, 296, 296)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblloc, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(159, 159, 159)
                        .addComponent(jLabel2)
                        .addGap(95, 95, 95)
                        .addComponent(txtSearchKeyWord1, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnSearchProduct1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(156, 156, 156)
                        .addComponent(jLabel3)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblloc, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(btnSearchProduct1)
                        .addComponent(txtSearchKeyWord1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(7, 7, 7)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 69, Short.MAX_VALUE)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnSearchProduct)
                    .addComponent(txtSearchKeyWord, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(backButton1))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSearchProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchProductActionPerformed
        String keyWord = txtSearchKeyWord.getText();
        refreshProductTable(keyWord);
    }//GEN-LAST:event_btnSearchProductActionPerformed

    private void btnSearchProduct1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchProduct1ActionPerformed
        // TODO add your handling code here:
        String keyWord = txtSearchKeyWord.getText();
        refreshInventoryTable(keyWord);
    }//GEN-LAST:event_btnSearchProduct1ActionPerformed

    private void backButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButton1ActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backButton1;
    private javax.swing.JButton btnSearchProduct;
    private javax.swing.JButton btnSearchProduct1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lblloc;
    private javax.swing.JTable tblInventoryItemCatalog;
    private javax.swing.JTable tblProductCatalog;
    private javax.swing.JTextField txtSearchKeyWord;
    private javax.swing.JTextField txtSearchKeyWord1;
    // End of variables declaration//GEN-END:variables
}
