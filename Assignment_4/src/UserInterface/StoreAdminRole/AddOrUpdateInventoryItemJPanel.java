/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.StoreAdminRole;

import Business.InventoryItem;
import Business.Product;
import Business.ProductCatalog;
import Business.Store;
import Business.StoreDirectory;
import java.awt.CardLayout;
import java.awt.Component;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author akki
 */
public class AddOrUpdateInventoryItemJPanel extends javax.swing.JPanel {

    /**
     * Creates new form AddOrUpdateInventoryItemJPanel
     */
   private StoreDirectory storeDirectory;
    private ProductCatalog productCatalog;
    private Store store;
    private JPanel userProcessContainer;
    
    
    public AddOrUpdateInventoryItemJPanel(JPanel userProcessContainer,ProductCatalog productCatalog, StoreDirectory storeDirectory, Store store ) {
        initComponents();
        this.userProcessContainer=userProcessContainer;
        this.storeDirectory=storeDirectory;
        this.productCatalog=productCatalog;
        this.store= store;
        
        lblloc.setText(store.getsLocation());
        populateInventoryTable();
        populateProductTable();
        
    }
    
    public void populateInventoryTable() {
        DefaultTableModel dtm1 = (DefaultTableModel) tblInventoryItemCatalog.getModel();
        
        dtm1.setRowCount(0);
        
            for (InventoryItem  i : store.getInventoryProductCatalog().getInventoryItemList()) {
                Object row[] = new Object[10];
                row[0] = i;
                row[1] = i.getProduct().getpID();
                row[2] = i.getProduct().getpCategory();
                row[3] = i.getProduct().getpBrand();
                row[4] = i.getProduct().getpWeight();
                row[5] = i.getSalesPrice();
                row[6] = i.getiAvailableQuantity();
                row[7] = i.getProduct().getpLicenseNumber();
                row[8] = i.getProduct().getpCustomerDemand();
                row[9] = i.getProduct().getpCustomerSatisfaction();    
                
                dtm1.addRow(row);
            }
        
    }
    
    private void refreshInventoryTable(String keyWord) {
        int rowCount = tblInventoryItemCatalog.getRowCount();
        DefaultTableModel model1 = (DefaultTableModel) tblInventoryItemCatalog.getModel();
        for (int i = rowCount - 1; i >= 0; i--) {
            model1.removeRow(i);
        }

        
            for (InventoryItem  i : store.getInventoryProductCatalog().getInventoryItemList()) {
                if (keyWord.equals(i.getProduct().getpName())) {
                Object row[] = new Object[10];
                row[0] = i;
                row[1] = i.getProduct().getpID();
                row[2] = i.getProduct().getpCategory();
                row[3] = i.getProduct().getpBrand();
                row[4] = i.getProduct().getpWeight();
                row[5] = i.getSalesPrice();
                row[6] = i.getiAvailableQuantity();
                row[7] = i.getProduct().getpLicenseNumber();
                row[8] = i.getProduct().getpCustomerDemand();
                row[9] = i.getProduct().getpCustomerSatisfaction();       

                    model1.addRow(row);
                }
            }
            if(tblInventoryItemCatalog.getRowCount()==0){
                JOptionPane.showMessageDialog(null, "No matches found", "INFO", JOptionPane.INFORMATION_MESSAGE);
                populateInventoryTable();
                txtSearchKeyWord.setText("");
            }
        

    }
    
    public void populateProductTable() {
        DefaultTableModel dtm = (DefaultTableModel) tblProductCatalog.getModel();
        
        dtm.setRowCount(0);
        
            for (Product product : productCatalog.getProductList()) {
                Object row[] = new Object[10];
                row[0] = product;
                row[1] = product.getpID();
                row[2] = product.getpCategory();
                row[3] = product.getpBrand();
                row[4] = product.getpWeight();
                row[5] = product.getpListedPrice();
                row[6] = product.getpAvailableQuantity();
                row[7] = product.getpLicenseNumber();
                row[8] = product.getpCustomerDemand();
                row[9] = product.getpCustomerSatisfaction();    
                
                dtm.addRow(row);
            }
        
    }
    
    private void refreshProductTable(String keyWord) {
        int rowCount = tblProductCatalog.getRowCount();
        DefaultTableModel model = (DefaultTableModel) tblProductCatalog.getModel();
        for (int i = rowCount - 1; i >= 0; i--) {
            model.removeRow(i);
        }

        
            for (Product product : productCatalog.getProductList()) {
                if (keyWord.equals(product.getpName())) {
                Object row[] = new Object[10];
                row[0] = product;
                row[1] = product.getpID();
                row[2] = product.getpCategory();
                row[3] = product.getpBrand();
                row[4] = product.getpWeight();
                row[5] = product.getpListedPrice();
                row[6] = product.getpAvailableQuantity();
                row[7] = product.getpLicenseNumber();
                row[8] = product.getpCustomerDemand();
                row[9] = product.getpCustomerSatisfaction();    

                    model.addRow(row);
                }
            }
            if(tblProductCatalog.getRowCount()==0){
                JOptionPane.showMessageDialog(null, "No matches found", "INFO", JOptionPane.INFORMATION_MESSAGE);
                populateProductTable();
                txtSearchKeyWord.setText("");
            }
        

    }   
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        lblloc = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblProductCatalog = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        txtSearchKeyWord = new javax.swing.JTextField();
        btnSearchProduct = new javax.swing.JButton();
        btnAddToInventory = new javax.swing.JButton();
        spnrQuantity = new javax.swing.JSpinner();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtSalesPrice = new javax.swing.JTextField();
        btnBack = new javax.swing.JButton();
        btnModifyQuantity = new javax.swing.JButton();
        btnRemoveOrderItem = new javax.swing.JButton();
        txtNewQuantity = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblInventoryItemCatalog = new javax.swing.JTable();
        txtSearchKeyWord1 = new javax.swing.JTextField();
        btnSearchProduct1 = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("Store:");

        lblloc.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        lblloc.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        tblProductCatalog.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Product Name", "Product ID", "Category", "Brand", "Weight", "Listed Price", "Available Quantity", "License Number", "Customer Demand", "Customer Satisfaction"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(tblProductCatalog);

        jLabel3.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Product Catalog:");

        btnSearchProduct.setText("Search Product By Name");
        btnSearchProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchProductActionPerformed(evt);
            }
        });

        btnAddToInventory.setText("ADD TO INVENTORY");
        btnAddToInventory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddToInventoryActionPerformed(evt);
            }
        });

        spnrQuantity.setModel(new javax.swing.SpinnerNumberModel());

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("Quantity:");

        jLabel6.setText("Sales Price");

        btnBack.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnBack.setText("<< Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        btnModifyQuantity.setText("Modify Quantity");
        btnModifyQuantity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModifyQuantityActionPerformed(evt);
            }
        });

        btnRemoveOrderItem.setText("Remove");
        btnRemoveOrderItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveOrderItemActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Store Inventory:");

        tblInventoryItemCatalog.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Product Name", "Product ID", "Category", "Brand", "Weight", "Sales Price", "Available Quantity", "License Number", "Customer Demand", "Customer Satisfaction"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(tblInventoryItemCatalog);

        btnSearchProduct1.setText("Search Product By Name");
        btnSearchProduct1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchProduct1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 795, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(157, 157, 157)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel1))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(lblloc, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(92, 92, 92)
                                        .addComponent(txtSearchKeyWord, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnSearchProduct))))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(24, 24, 24)
                                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(202, 202, 202)
                                .addComponent(txtNewQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(20, 20, 20)
                                .addComponent(btnModifyQuantity)
                                .addGap(26, 26, 26)
                                .addComponent(btnRemoveOrderItem))
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 795, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(149, 149, 149)
                                .addComponent(jLabel2)
                                .addGap(103, 103, 103)
                                .addComponent(txtSearchKeyWord1, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnSearchProduct1)))))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(181, 181, 181)
                .addComponent(jLabel6)
                .addGap(18, 18, 18)
                .addComponent(txtSalesPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(jLabel5)
                .addGap(12, 12, 12)
                .addComponent(spnrQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42)
                .addComponent(btnAddToInventory)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblloc, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtSearchKeyWord, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnSearchProduct))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtSalesPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel6))
                    .addComponent(jLabel5)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(spnrQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnAddToInventory)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 56, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnSearchProduct1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(7, 7, 7)
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtSearchKeyWord1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtNewQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnModifyQuantity)
                                .addComponent(btnRemoveOrderItem)))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnBack)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSearchProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchProductActionPerformed
        String keyWord = txtSearchKeyWord.getText();
        refreshProductTable(keyWord);
    }//GEN-LAST:event_btnSearchProductActionPerformed

    private void btnAddToInventoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddToInventoryActionPerformed
        // TODO add your handling code here:

        // TODO add your handling code here:
//        int selectedRowCount1 = tblInventoryItemCatalog.getSelectedRowCount();
//        if (selectedRowCount1 !=1) {
//            JOptionPane.showMessageDialog(null, "select one row from the inventoryItem table!");
//            return;
//        }
        
        int selectedRow = tblProductCatalog.getSelectedRow();
        Product selectedProduct;
        int salesPrice = 0;
        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(this, "Select one row", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        } else {
            selectedProduct = (Product) tblProductCatalog.getValueAt(selectedRow, 0);
        }

        try {
            salesPrice = Integer.parseInt(txtSalesPrice.getText());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Enter valid sales price", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        if (salesPrice < selectedProduct.getpListedPrice()) {
            JOptionPane.showMessageDialog(this, "Sales Price should be more than listed price", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        int listedprice=selectedProduct.getpListedPrice();
        int fetchedQty = (Integer) spnrQuantity.getValue();
        if (fetchedQty <= 0) {
            JOptionPane.showMessageDialog(this, "Selected atlest 1 quantity", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        } else if (fetchedQty <= selectedProduct.getpAvailableQuantity()) {
            boolean alreadyPresent = false;
            for (InventoryItem inventoryItem : store.getInventoryProductCatalog().getInventoryItemList()) {
                if (inventoryItem.getProduct() == selectedProduct) {
                    int oldAvail = selectedProduct.getpAvailableQuantity();
                    int newAvail = oldAvail - fetchedQty;
                    selectedProduct.setpAvailableQuantity(newAvail);
                    inventoryItem.setiAvailableQuantity(fetchedQty + inventoryItem.getiAvailableQuantity());
                    inventoryItem.setSalesPrice(salesPrice);
                    alreadyPresent = true;
                    populateInventoryTable();
                    populateProductTable();
                    break;
                }
            }

            if (!alreadyPresent) {
                int oldAvail = selectedProduct.getpAvailableQuantity();
                int newAvail = oldAvail - fetchedQty;
                selectedProduct.setpAvailableQuantity(newAvail);
                store.getInventoryProductCatalog().addInventoryItem(selectedProduct, fetchedQty, listedprice,salesPrice);
                populateInventoryTable();
                populateProductTable();
            }
        } else {
            JOptionPane.showMessageDialog(this, "Quantity > Availability!!", "Warning", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnAddToInventoryActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
//        if (order.getInventoryItemList().size() > 0) {
//            ArrayList<InventoryItem> orderList = order.getInventoryItemList();
//            for (InventoryItem inventoryItem : orderList) {
//                Product p = inventoryItem.getProduct();
//                p.setpAvailableQuantity(inventoryItem.getiAvailableQuantity() + p.getpAvailableQuantity());
//            }
//            order.getInventoryItemList().removeAll(orderList);
//        }
        userProcessContainer.remove(this);
        Component[] componentArray = userProcessContainer.getComponents();
        Component c = componentArray[componentArray.length - 1];
        ManageStoreInventoryJPanel ms = (ManageStoreInventoryJPanel) c;
        ms.populateInventoryTable();
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnModifyQuantityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModifyQuantityActionPerformed
        // TODO add your handling code here:
        int selectedRow = tblInventoryItemCatalog.getSelectedRow();
        //Product selectedProduct;
        //int salesPrice=0;
        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(this, "Select a row", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        if (!txtNewQuantity.getText().isEmpty() ) {
            // && !txtNewQuantity.getText().equals("0")
            InventoryItem inventoryItem = (InventoryItem) tblInventoryItemCatalog.getValueAt(selectedRow, 0);
            int currentAvail = inventoryItem.getProduct().getpAvailableQuantity();
            int oldQty = inventoryItem.getiAvailableQuantity();
            int newQty = Integer.parseInt(txtNewQuantity.getText());
            if (newQty > (currentAvail + oldQty)) {
                JOptionPane.showMessageDialog(null, "Quantity is more than the availability");
                //return;
            } else {
                if (newQty < 0) {
                    JOptionPane.showMessageDialog(null, "Invalid qty");
                    return;
                }
                inventoryItem.setiAvailableQuantity(newQty);
                inventoryItem.getProduct().setpAvailableQuantity(currentAvail + (oldQty - newQty));
                populateInventoryTable();
                populateProductTable();
            }
        } else {
            JOptionPane.showMessageDialog(null, "Quantity cannot be zero!!");
        }
    }//GEN-LAST:event_btnModifyQuantityActionPerformed

    private void btnRemoveOrderItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveOrderItemActionPerformed
        int selectedRowCount = tblInventoryItemCatalog.getSelectedRowCount();
        if (selectedRowCount !=1) {
            JOptionPane.showMessageDialog(null, "select one row from the inventoryItem table!");
            return;
        }

        int row = tblInventoryItemCatalog.getSelectedRow();
        if (row < 0) {
            JOptionPane.showMessageDialog(null, "Failed to retrive selected row");
            return;
        }

        InventoryItem oi = (InventoryItem) tblInventoryItemCatalog.getValueAt(row, 0);
        int oldQuantity = oi.getProduct().getpAvailableQuantity();
        int orderQuantity = oi.getiAvailableQuantity();
        int newQuantity = oldQuantity + orderQuantity;
        oi.getProduct().setpAvailableQuantity(newQuantity);
        store.getInventoryProductCatalog().removeInventoryItem(oi);
        JOptionPane.showMessageDialog(null, "The order item of " + orderQuantity + "of " + oi + " has been removed.");
        populateInventoryTable();
        populateProductTable();
    }//GEN-LAST:event_btnRemoveOrderItemActionPerformed

    private void btnSearchProduct1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchProduct1ActionPerformed
        // TODO add your handling code here:
        String keyWord = txtSearchKeyWord1.getText();
        refreshInventoryTable(keyWord);
    }//GEN-LAST:event_btnSearchProduct1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddToInventory;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnModifyQuantity;
    private javax.swing.JButton btnRemoveOrderItem;
    private javax.swing.JButton btnSearchProduct;
    private javax.swing.JButton btnSearchProduct1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lblloc;
    private javax.swing.JSpinner spnrQuantity;
    private javax.swing.JTable tblInventoryItemCatalog;
    private javax.swing.JTable tblProductCatalog;
    private javax.swing.JTextField txtNewQuantity;
    private javax.swing.JTextField txtSalesPrice;
    private javax.swing.JTextField txtSearchKeyWord;
    private javax.swing.JTextField txtSearchKeyWord1;
    // End of variables declaration//GEN-END:variables
}
